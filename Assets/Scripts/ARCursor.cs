using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARCursor : MonoBehaviour
{
    public GameObject cursorChildObject;
    public ARRaycastManager raycastManager;

    public bool useCursor = true;

    private void Start()
    {
        cursorChildObject.SetActive(useCursor);
    }

    private void Update()
    {
        if (useCursor)
        {
            UpdateCursor();
        }

        if (Input.GetMouseButtonDown(0))
        {
            SpawnBitmoji();
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            SpawnBitmoji();
        }
    }

    private void SpawnBitmoji()
    {
        var bitmoji = GameObject.Find("Bitmoji");

        if (bitmoji != null)
        {
            bitmoji.SetActive(false);
            var spawnedBitmoji = Instantiate(bitmoji, transform.position, transform.rotation);
            spawnedBitmoji.SetActive(true);
        }
    }

    private void UpdateCursor()
    {
        var screenPosition = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        raycastManager.Raycast(screenPosition, hits, TrackableType.Planes);

        if (hits.Count > 0)
        {
            transform.position = hits[0].pose.position;
            transform.rotation = hits[0].pose.rotation;
        }
    }
}
